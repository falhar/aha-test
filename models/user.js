const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.loginSession, { foreignKey: 'userId' });
    }
  }
  User.init(
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      isVerify: { type: DataTypes.BOOLEAN, defaultValue: false },
      googleId: DataTypes.STRING,
      fbId: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'user',
    }
  );
  return User;
};
