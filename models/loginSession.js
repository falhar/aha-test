const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class LoginSession extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      LoginSession.belongsTo(models.user, { foreignKey: 'userId' });
    }
  }
  LoginSession.init(
    {
      userId: DataTypes.INTEGER,
      finish: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: 'loginSession',
      createdAt: 'start',
      updatedAt: false,
    }
  );
  return LoginSession;
};
