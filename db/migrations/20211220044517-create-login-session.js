module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('loginSessions', {
      id: { allowNull: false, autoIncrement: true, primaryKey: true, type: Sequelize.INTEGER },
      userId: { type: Sequelize.INTEGER, references: { model: 'users', key: 'id' } },
      start: { allowNull: false, type: Sequelize.DATE },
      finish: { type: Sequelize.DATE },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('loginSessions');
  },
};
