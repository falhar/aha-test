const path = require('path');
const swig = require('swig-templates');
const nodemailer = require('nodemailer');

const config = require('../config');
const logger = require('../tools/logger');

module.exports = {
  nodeMailerTransport() {
    const transport = nodemailer.createTransport({
      host: config.mail.host,
      port: config.mail.port,
      secure: false,
      ignoreTLS: false,
      auth: {
        user: config.mail.user,
        pass: config.mail.pass,
      },
    });
    return transport;
  },

  async nodeMailerActivation({ name, email, token }) {
    const template = swig.compileFile(path.resolve(__dirname, 'email-templates/email-confirmation-v1.html'));
    const emailHtml = template({
      name,
      email,
      token,
      domainClient: config.frontend.web.host,
    });
    const info = this.nodeMailerTransport().sendMail({
      from: 'Aha Test <gimmail461@gmail.com>',
      to: email,
      subject: 'Email Activation',
      html: emailHtml,
    });

    logger.info(`nodemailer.activation.sent: ${info.messageId}`);
  },
};
