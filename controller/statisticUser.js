const { Op } = require('sequelize');
const { user, loginSession } = require('../models');

module.exports = {
  async statisticUserLogin(req, res) {
    const totalUser = await user.count();
    const activeToday = await loginSession.count({
      distinct: true,
      col: 'userId',
      where: {
        start: {
          [Op.gte]: new Date(new Date().setDate(new Date().getDate() - 1)),
        },
        finish: null,
      },
    });
    const last7day = await loginSession.count({
      where: {
        start: {
          [Op.gte]: new Date(Date.now() - new Date().setDate(new Date().getDate() - 7)),
        },
      },
    });
    const avgLast7Day = Math.round(last7day / 7);
    res.status(200).json({
      message: 'Success',
      totalUser,
      activeToday,
      avgLast7Day,
    });
  },
};
