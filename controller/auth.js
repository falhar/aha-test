const randToken = require('rand-token');
const fs = require('fs');
const path = require('path');
const bcrypt = require('bcrypt');
const { Op } = require('sequelize');
const config = require('../config');
const ApiError = require('../errors/api-error');
const { user, loginSession, userVerify } = require('../models');
const tokenGenerator = require('../services/token-generator');
const emailService = require('./email');

class AuthController {
  async registerCon(req, res) {
    const { name, email, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, config.app.password_salt);
    await user.create({
      name: name.trim(),
      email,
      password: hashedPassword,
    });
    const usVer = await userVerify.create({ email, code: randToken.generate(64) });
    await emailService.nodeMailerActivation({ name, email, token: usVer.code }).catch((err) => {
      throw new ApiError(500, err.message, err);
    });
    res.status(200).json({
      message: 'Check email to verify',
    });
  }

  async loginCon(req, res) {
    const { email, password } = req.body;

    const userLog = await user.findOne({ where: { email } });
    if (!userLog) {
      throw new ApiError(422, 'These credentials do not match our records.');
    }

    const validate = await bcrypt.compare(password, userLog.password);
    if (!validate) {
      throw new ApiError(422, `These credentials doesn't match our records.`);
    }
    if (!userLog.isVerify) {
      throw new ApiError(401, 'Email not verified', { email });
    }
    const access_token = await tokenGenerator(userLog.id, config.jwt.secretKey, config.jwt.expiresIn);
    await loginSession.create({
      userId: userLog.id,
      start: new Date(),
    });

    res.status(200).json({
      message: 'Login success!',
      access_token,
    });
  }

  async login3rd(req, res) {
    const access_token = await tokenGenerator(req.user.id, config.jwt.secretKey, config.jwt.expiresIn);
    await loginSession.create({
      userId: req.user.id,
      start: new Date(),
    });

    let resHtml = `<html>
                    <head>
                      <title>Main</title>
                    </head>
                    <body></body>
                    <script>
                      res = %value%; window.opener.postMessage(res, "*");
                      window.close();
                    </script>
                  </html>`;
    resHtml = resHtml.replace(
      '%value%',
      JSON.stringify({
        message: 'Login success!',
        access_token,
      })
    );
    res.status(200).send(resHtml);
  }

  async sendVerifyEmail(req, res) {
    const { email } = req.body;
    const checkVerUs = await user.count({ where: { email, isVerify: true } });
    if (checkVerUs) {
      throw new ApiError(500, 'Email Verified');
    }
    const checkCodeVer = await userVerify.findOne({ where: { email } });
    let code;
    if (!checkCodeVer) {
      code = randToken.generate(64);
      await userVerify.create({ email, code });
    } else {
      const checkUser = await userVerify.findOne({
        where: { email },
      });
      code = checkUser.code;
    }
    const userVer = await user.findOne({ where: { email } });
    await emailService.nodeMailerActivation({ name: userVer.name, email, token: code }).catch((err) => {
      throw new ApiError(500, err.message, err);
    });

    res.status(200).json({
      message: 'Email sent!',
    });
  }

  async confirmEmailPage(req, res) {
    const showPageNotFound = () => {
      res.writeHead(404);
      res.write('Sorry, The page you are looking for is not found');
      res.end();
    };
    const { token, email } = req.query;
    if (!token || !email) {
      return showPageNotFound();
    }

    const checkCodeVer = await userVerify.count({ where: { email, code: token } });
    if (!checkCodeVer) {
      return showPageNotFound();
    }

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.readFile(path.join(__dirname, 'views/user-confirmation.html'), function (error, data) {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write(data);
      res.end();
    });
  }

  async verifyEmail(req, res) {
    const { email, code } = req.body;
    const checkCodeVer = await userVerify.count({ where: { email, code } });
    if (!checkCodeVer) {
      throw new ApiError(500, 'Internal Server Error');
    }
    await user.update({ isVerify: true }, { where: { email } });
    await userVerify.destroy({ where: { email, code } });
    res.status(200).json({
      message: 'Email Verified!',
    });
  }

  async logoutCon(req, res) {
    const checkSession = await loginSession.findOne({
      where: {
        finish: null,
        userId: req.user.id,
        start: {
          [Op.gte]: new Date(Date.now() - 24 * 60 * 60 * 1000),
        },
      },
    });
    if (!checkSession) {
      throw new ApiError(500, 'Logout failed');
    }
    await checkSession.update({ finish: new Date() });
    res.status(200).json({
      message: 'Logout success',
    });
  }

  // eslint-disable-next-line no-unused-vars
  async cbFailed3rd(req, res) {
    throw new ApiError(500, 'Internal Server Error');
  }
}

module.exports = new AuthController();
