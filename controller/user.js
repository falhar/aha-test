const bcrypt = require('bcrypt');
const config = require('../config');
const ApiError = require('../errors/api-error');
const { user, loginSession, Sequelize } = require('../models');

module.exports = {
  async getAll(req, res) {
    const data = await user.findAll({
      attributes: {
        include: [
          [Sequelize.fn('COUNT', Sequelize.col('loginSessions.id')), 'timesLogin'],
          [Sequelize.fn('MAX', Sequelize.col('loginSessions.start')), 'lastLogin'],
        ],
      },
      include: { model: loginSession, attributes: [] },
      group: [['id']],
    });
    res.status(200).json({
      message: 'Success',
      data,
    });
  },
  async getOne(req, res) {
    const data = await user.findByPk(req.user.id);
    res.status(200).json({
      message: 'Success',
      data,
    });
  },
  async update(req, res) {
    const { name } = req.body;
    await user.update({ name }, { where: { id: req.user.id } });
    const data = await user.findByPk(req.user.id);
    res.status(200).json({
      message: 'Success',
      data,
    });
  },
  async changePassword(req, res) {
    const { newPassword, oldPassword } = req.body;
    const checkUser = await user.findByPk(req.user.id);
    if (!checkUser.password && (checkUser.googleId || checkUser.fbId)) {
      throw new ApiError(422, 'Password is not set, You may login using Google or Facebook');
    }
    const validate = await bcrypt.compare(oldPassword, checkUser.password);
    if (!validate) {
      throw new ApiError(422, `Current Password Does'nt Matched Our Record`);
    }
    const hashedPassword = await bcrypt.hash(newPassword, config.app.password_salt);
    await user.update({ password: hashedPassword }, { where: { id: req.user.id } });
    res.status(200).json({ message: 'Success' });
  },
};
