const { body } = require('express-validator');
const handleValidationError = require('./validator');

module.exports = {
  updateVal: [
    body('name')
      .notEmpty()
      .withMessage('name required')
      .matches(/^[a-zA-Z ']{3,200}$/),
    handleValidationError,
  ],
  changePasswordVal: [
    body('oldPassword').notEmpty().withMessage('Current Password required'),
    body('newPassword')
      .notEmpty()
      .withMessage('New Password required')
      .isStrongPassword({ minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1, minLength: 8 }),
    handleValidationError,
  ],
};
