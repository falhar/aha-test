const { body } = require('express-validator');
const { user } = require('../../models');
const handleValidationError = require('./validator');

module.exports = {
  registerVal: [
    body('name')
      .notEmpty()
      .withMessage('name required')
      .matches(/^[a-zA-Z ']{3,200}$/),
    body('email')
      .notEmpty()
      .withMessage('email required')
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail()
      .withMessage('must be an email address')
      .custom(async (email) => {
        const count = await user.count({ where: { email } });
        if (count === 1) {
          throw new Error('email already used!');
        } else {
          return true;
        }
      }),
    body('password')
      .notEmpty()
      .withMessage('password required')
      .isStrongPassword({ minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1, minLength: 8 }),
    handleValidationError,
  ],
  loginVal: [
    body('email')
      .notEmpty()
      .withMessage('email required')
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail()
      .withMessage('must be an email address'),
    body('password').notEmpty().withMessage('password required').isString(),
    handleValidationError,
  ],
  sendVerifyVal: [
    body('email')
      .notEmpty()
      .withMessage('email required')
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail()
      .withMessage('must be an email address')
      .custom(async (email) => {
        const checkUser = await user.count({ where: { email } });
        if (!checkUser) {
          throw new Error('User Not Found');
        }
        return true;
      }),
    handleValidationError,
  ],
  verifyVal: [
    body('email')
      .notEmpty()
      .withMessage('email required')
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail()
      .withMessage('must be an email address')
      .custom(async (email) => {
        const checkUser = await user.count({ where: { email } });
        if (!checkUser) {
          throw new Error('User Not Found');
        }
        return true;
      }),
    body('code').notEmpty().withMessage('code required').isString().isLength({ min: 64, max: 64 }),
    handleValidationError,
  ],
};
