const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FBStrategy = require('passport-facebook').Strategy;
const { user } = require('../../models');
const config = require('../../config');

const jwtPassport = new JWTstrategy(
  {
    secretOrKey: config.jwt.secretKey, // It must be same with secret key when created token
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(), // It will extract token from req.header('Authorization')
  },
  async (token, done) => {
    try {
      const userLogin = await user.findByPk(token.user.id);
      if (!userLogin) {
        return done(null, false);
      }

      return done(null, token.user);
    } catch (e) {
      return done(e, false);
    }
  }
);

const googlePassport = new GoogleStrategy(
  {
    clientID: config.passport.googleClientId,
    clientSecret: config.passport.googleClientSecret,
    callbackURL: `${config.passport.baseGoogleCb}/api/auth/google/callback`,
  },
  async (accessToken, refreshToken, profile, cb) => {
    try {
      let userLogin = await user.findOne({ where: { googleId: profile.id } });
      if (!userLogin) {
        userLogin = await user.findOne({ where: { email: profile.emails[0].value } });
        if (!userLogin) {
          userLogin = await user.create({
            name: profile.displayName,
            email: profile.emails[0].value,
            isVerify: true,
            googleId: profile.id,
          });
        } else {
          await userLogin.update({ googleId: profile.id, isVerify: true });
        }
      }

      return cb(null, userLogin);
    } catch (e) {
      return cb(e, false);
    }
  }
);

const fbPassport = new FBStrategy(
  {
    clientID: config.passport.fbClientId,
    clientSecret: config.passport.fbClientSecret,
    callbackURL: `${config.passport.baseFbCb}/api/auth/facebook/callback`,
    profileFields: ['id', 'email', 'displayName'],
  },
  async (accessToken, refreshToken, profile, cb) => {
    try {
      let userLogin = await user.findOne({ where: { fbId: profile.id } });
      if (!userLogin) {
        userLogin = await user.findOne({ where: { email: profile.emails[0].value } });
        if (!userLogin) {
          userLogin = await user.create({
            name: profile.displayName,
            email: profile.emails[0].value,
            isVerify: true,
            fbId: profile.id,
          });
        } else {
          await userLogin.update({ fbId: profile.id, isVerify: true });
        }
      }

      return cb(null, userLogin);
    } catch (e) {
      return cb(e, false);
    }
  }
);

module.exports = { jwtPassport, googlePassport, fbPassport };
