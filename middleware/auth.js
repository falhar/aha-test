const passport = require('passport');
const ApiError = require('../errors/api-error');

const auth = (req, res, next) => {
  return new Promise((resolve, reject) => {
    const authenticate = passport.authenticate('jwt', { session: false }, async (err, user) => {
      if (err || !user) {
        return reject(new ApiError(401, 'Unauthorized'));
      }

      req.user = user;

      resolve();
    });

    authenticate(req, res, next);
  })
    .then(() => next())
    .catch((err) => next(err));
};

const authGoogle = (req, res, next) => {
  return new Promise((resolve, reject) => {
    const authenticate = passport.authenticate(
      'google',
      { session: false, scope: ['profile', 'email'], failureRedirect: '/api/auth/failed' },
      async (err, user) => {
        if (err || !user) {
          return reject(new ApiError(401, 'Unauthorized'));
        }

        req.user = user;

        resolve();
      }
    );

    authenticate(req, res, next);
  })
    .then(() => next())
    .catch((err) => next(err));
};

const authFb = (req, res, next) => {
  return new Promise((resolve, reject) => {
    const authenticate = passport.authenticate(
      'facebook',
      { session: false, failureRedirect: '/api/auth/failed', scope: ['email'] },
      async (err, user) => {
        if (err || !user) {
          return reject(new ApiError(401, 'Unauthorized', { err, user }));
        }

        req.user = user;

        resolve();
      }
    );

    authenticate(req, res, next);
  })
    .then(() => next())
    .catch((err) => next(err));
};

module.exports = { auth, authGoogle, authFb };
