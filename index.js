const app = require('./app');
const config = require('./config');
const logger = require('./tools/logger');

const port = config.app.port || 3000;

app.listen(port, () => {
  logger.info(`program running on port ${port}`);
});
