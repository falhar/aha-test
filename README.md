# AHA's Test API

## Requirements

1. Nodejs >= 14
2. Mysql 5.7
3. Npm global packages
   - sequelize-cli

## Installation

1. Clone Project
2. Install node dependencies

```
npm Install
or
npm install --only=prod
```

3. Create file .env from .env.example

```
cp .env.example .env
```

4. Adjust variable `.env`
   - `DB_HOST`, `DB_USER`, `DB_PASS`, `DB_DATABASE` as environment variable
5. Run migration

```
sequelize db:migrate
```

6. Run the app

```
npm run dev
```
