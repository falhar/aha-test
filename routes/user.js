const router = require('express').Router();
const { getOne, update, getAll, changePassword } = require('../controller/user');
const { auth } = require('../middleware/auth');
const { updateVal, changePasswordVal } = require('../middleware/validators/user');
const catchAsync = require('./catch-async');

router.get('/users', auth, catchAsync(getAll));
router.get('/user', auth, catchAsync(getOne));
router.put('/user', auth, updateVal, catchAsync(update));
router.put('/user/password', auth, changePasswordVal, catchAsync(changePassword));

module.exports = router;
