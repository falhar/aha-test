const router = require('express').Router();
const { statisticUserLogin } = require('../controller/statisticUser');
const { auth } = require('../middleware/auth');
const catchAsync = require('./catch-async');

router.get('/statistic', auth, catchAsync(statisticUserLogin));

module.exports = router;
