const router = require('express').Router();
const {
  registerCon,
  loginCon,
  logoutCon,
  cbFailed3rd,
  login3rd,
  verifyEmail,
  sendVerifyEmail,
  confirmEmailPage,
} = require('../controller/auth');
const { authGoogle, authFb, auth } = require('../middleware/auth');
const { registerVal, loginVal, verifyVal, sendVerifyVal } = require('../middleware/validators/auth');
const catchAsync = require('./catch-async');

router.post('/auth/register', registerVal, catchAsync(registerCon));
router.post('/auth/login', loginVal, catchAsync(loginCon));
router.post('/auth/logout', auth, catchAsync(logoutCon));
router.post('/auth/confirm', sendVerifyVal, catchAsync(sendVerifyEmail));
router.get('/auth/confirmation', catchAsync(confirmEmailPage));
router.put('/auth/verify', verifyVal, catchAsync(verifyEmail));
router.get('/auth/google', authGoogle);
router.get('/auth/google/callback', authGoogle, catchAsync(login3rd));
router.get('/auth/facebook', authFb);
router.get('/auth/facebook/callback', authFb, catchAsync(login3rd));
router.get('/auth/failed', catchAsync(cbFailed3rd));

module.exports = router;
