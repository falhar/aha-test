const express = require('express');
const passport = require('passport');
const cors = require('cors');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const helmet = require('helmet');

const app = express();
const config = require('./config');
const morgan = require('./tools/morgan');
const errorMiddleware = require('./middleware/error');
const ApiError = require('./errors/api-error');
const authRoute = require('./routes/auth');
const user = require('./routes/user');
const statisticUser = require('./routes/userStatistic');
const { jwtPassport, googlePassport, fbPassport } = require('./middleware/passport-strategies/jwt-passport');

if (config.app.env !== 'test') {
  app.set('trust proxy', true);
  app.use(morgan.logSuccessRequest);
  app.use(morgan.logErrorRequest);
}
app.use(cors());
app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ extended: true, limit: '5mb' }));
app.use(express.static(`${__dirname}/public`));
app.use(xss());
const limiter = rateLimit({
  windowMs: 60 * 1000, // 1 min
  max: 500,
});

app.use(limiter);
app.use(hpp());
app.use(helmet({ contentSecurityPolicy: false }));
app.use(passport.initialize());
passport.use('jwt', jwtPassport);
passport.use('google', googlePassport);
passport.use('facebook', fbPassport);

// routes
app.use('/api', authRoute);
app.use('/api', user);
app.use('/api', statisticUser);

app.use((req, res, next) => {
  next(new ApiError(404, 'Endpoint Not Found!'));
});

app.use(errorMiddleware);

module.exports = app;
