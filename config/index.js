const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });
const db = require('./db');

module.exports = {
  app: {
    port: process.env.PORT,
    env: process.env.NODE_ENV,
    password_salt: 12,
  },
  db,
  jwt: {
    secretKey: process.env.JWT_SECRET_KEY,
    expiresIn: process.env.JWT_EXP_TOKEN,
  },
  sendgrid: {
    key: process.env.SENDGRID_API_KEY,
  },
  mail: {
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    user: process.env.MAIL_NAME,
    pass: process.env.MAIL_PASS,
    receiver: process.env.MAIL_RECEIVER,
  },
  frontend: {
    web: {
      host: process.env.WEB_CLIENT_HOST,
    },
  },
  redis: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST,
  },
  passport: {
    googleClientId: process.env.GOOGLE_CLIENT_ID,
    googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
    baseGoogleCb: process.env.CALLBACK_GOOGLE,
    fbClientId: process.env.FACEBOOK_APP_ID,
    fbClientSecret: process.env.FACEBOOK_APP_SECRET,
    baseFbCb: process.env.CALLBACK_FB,
  },
};
